require('angular-material/angular-material');
require('angular-material/angular-material.css');

require('./material-icons.css');
require('./setup-font.css');

var common = angular.module('app.common', [
	'ngMaterial'
]);

require('./components/app-base');
require('./services/national-id-manager');

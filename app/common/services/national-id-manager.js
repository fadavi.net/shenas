/* global angular, require */

var common = angular.module('app.common');

common.service('appNationalIdManager', AppNationalIdManager);
function AppNationalIdManager () {
	var appNationalIdManager = this;
	
	var lib = {};
	lib.cityCode = require('../../lib/city-code');
	lib.nationalIdFormatter = require('../../lib/national-id-formatter');
	lib.validateNationalId = require('../../lib/validate-national-id');

	appNationalIdManager.parse = lib.nationalIdFormatter.parse;

	appNationalIdManager.parseParts = lib.nationalIdFormatter.parseParts;
	
	appNationalIdManager.findLocation = lib.cityCode.find;

	appNationalIdManager.validate = lib.validateNationalId;	
}

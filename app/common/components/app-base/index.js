var common = angular.module('app.common');

common.component('appBase', {
	template: require('./app-base.html'),
	controller: AppBaseCtrl,
	bindings: { layout: '@', flex: '@' }
});
AppBaseCtrl.$inject = ['$scope', '$window', '$mdSidenav', '$mdDialog'];
function AppBaseCtrl ($scope, $window, $mdSidenav, $mdDialog) {
	var self = this;
	
	self.$onInit = function () {
		!self.layout && (self.layout = 'column');
		!self.flex && (self.flex = '');
	};

	$scope.exit = function (e) {
		$scope.toggleMainSidenav();

		var confirm = $mdDialog.confirm()
				.title('خروج')
				.textContent('واقعا می‌خوای بری؟')
				.targetEvent(e)
				.ok('بله')
				.cancel('خیر');

		$mdDialog.show(confirm)
			.then($window.close, angular.noop);
	};

	$scope.toggleMainSidenav = function () {
		$mdSidenav('main-sidenav').toggle();
	};
	
}

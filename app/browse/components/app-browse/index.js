var browse = angular.module('app.browse');

browse.component('appBrowse', {
	template: require('./app-browse.html'),
	controller: BrowseCtrl,
	bindings: { layout: '@', flex: '@' }
});
BrowseCtrl.$inject = [];
function BrowseCtrl () {
	var self = this;

	self.$onInit = function () {
		!self.layout && (self.layout = 'column');
		!self.flex && (self.flex = '');
	};
}

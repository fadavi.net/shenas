
function parseDigits (number) {
	if(!Number.isSafeInteger(number) || number <= 0) {
		return [0];
	}
	
	const result = [];

	while (number) {
		result.push(number % 10);
		number = Math.floor(number / 10);
	}

	return result.reverse();
}

module.exports = parseDigits;

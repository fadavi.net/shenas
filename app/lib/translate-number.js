const allDigits = zero => (new Array(10)).fill(0)
			.map((_, i) => String.fromCharCode(zero + i));

const ENGLISH_ZERO = 48;
const PERSIAN_ZERO = 1776;
const ARABIC_ZERO = 1632;
const ENGLISH_DIGITS = allDigits(ENGLISH_ZERO);
const PERSIAN_DIGITS = allDigits(PERSIAN_ZERO);
const ARABIC_DIGITS = allDigits(ARABIC_ZERO);

function asDigit (char) {
	let digit = ENGLISH_DIGITS.indexOf(char);
	~digit || (digit = PERSIAN_DIGITS.indexOf(char));
	~digit || (digit = ARABIC_DIGITS.indexOf(char));
	return digit;
}

function translateChar (digits, char) {
	const digit = asDigit(char);
	return ~digit ? digits[digit] : char;
}

function translate (digits, str) {
	if (str == null || (typeof str === 'number' && !Number.isFinite(str))) {
		return str;
	}
	
	str += '';
	
	let result = '';
	for (let char of str) {
		result += translateChar(digits, char);
	}

	return result;
}

const toEnglish = char => translate(ENGLISH_DIGITS, char);
const toPersian = char => translate(PERSIAN_DIGITS, char);
const toArabic = char => translate(ARABIC_DIGITS, char);

module.exports = {toEnglish, toPersian, toArabic};

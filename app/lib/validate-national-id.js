function validateNationalId (digits) {
	digits = digits.reverse();

	const checksum = digits.shift() | 0;
	const sum = digits
				.map(digit => digit | 0)
				.map((digit, coeff) => digit * (coeff + 2))
				.reduce((x, sum) => x + sum, 0);
	const mod = sum % 11;
		
	if (mod < 3 && checksum === mod) {
		return true;
	}
	
	if (mod + checksum === 11) {
		return true;
	}

	return false;
}

module.exports = validateNationalId;

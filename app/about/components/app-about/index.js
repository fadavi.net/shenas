var about = angular.module('app.about');

about.component('appAbout', {
	template: require('./app-about.html'),
	controller: AboutCtrl,
	bindings: { layout: '@', flex: '@' }
});
AboutCtrl.$inject = [];
function AboutCtrl () {
	var self = this;

	self.$onInit = function () {
		!self.layout && (self.layout = 'column');
		!self.flex && (self.flex = '');
	};
}

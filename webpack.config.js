const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {

	mode: 'production',
	devtool: 'source-map',

	context: __dirname,
	
	entry: {
		app: path.join(__dirname, 'app', 'index.js')
	},

	output: {
		filename: '[name].bundle.js',
		path: path.join(__dirname, 'release')
	},

	module: {
		rules: [

			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					'style-loader',
					'css-loader'
				]
			},

			{
				test: /\.html$/,
				use: [
					'raw-loader'
				]
			},

			{
				test: /\.(png|woff|woff2|eot|ttf|svg)$/,
				use: [
					'url-loader?limit=100000'
				]
			}
			
		]
	},

	plugins: [
		new HtmlWebpackPlugin({
			template: 'app/index.html'
		})
	]
};
